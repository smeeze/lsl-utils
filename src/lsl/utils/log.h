#pragma once

// fmt
#include <fmt/format.h>
#include <fmt/ranges.h>

namespace lsl {
namespace utils {
class log {
public:
  enum class level { //
    console,         //
    debug,           //
    info,            //
    warning,         //
    error,           //
    fatal,           //
    todo             //
  };

  template <typename... Args> static void console(Args &&...args) {
    _log(level::console, fmt::format(std::forward<Args>(args)...));
  }
  template <typename... Args>
  static void debug(Args &&...args [[maybe_unused]]) {
#ifndef NDEBUG
    _log(level::debug, fmt::format(std::forward<Args>(args)...));
#endif
  }
  template <typename... Args> static void info(Args &&...args) {
    _log(level::info, fmt::format(std::forward<Args>(args)...));
  }
  template <typename... Args> static void warning(Args &&...args) {
    _log(level::warning, fmt::format(std::forward<Args>(args)...));
  }
  template <typename... Args> static void error(Args &&...args) {
    _log(level::error, fmt::format(std::forward<Args>(args)...));
  }
  template <typename... Args> static void fatal(Args &&...args) {
    _log(level::fatal, fmt::format(std::forward<Args>(args)...));
  }
  template <typename... Args>
  static void todo(Args &&...args [[maybe_unused]]) {
#ifndef NDEBUG
    _log(level::todo, fmt::format(std::forward<Args>(args)...));
#endif
  }

private:
  static void _log(level, const std::string &);
};
} // namespace core
} // namespace erptools
