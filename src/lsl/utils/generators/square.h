#pragma once

namespace lsl {
namespace utils {
namespace generators {
class square {
public:
  struct context {
    bool trigger_increment;
    int trigger_code;
    float scaling;
  };
  square(context &&ctx) : _context(ctx) {}
  void operator()(int, int, int, float *, int, int &);

private:
  context _context;
};
} // namespace generators
} // namespace utils
} // namespace lsl
