// lsl utils
#include <lsl/utils/generators/iota.h>
///////////////////////////////////////////////////////////////////////////////
void lsl::utils::generators::iota::operator()(int, int, int,
                                              float *channel_data,
                                              int channel_count, int &trigger) {
  trigger = 0;
  for (int c = 0; c < channel_count; ++c) {
    channel_data[c] = 0.5f + (float)c - (float)channel_count * 0.5;
  }
}
