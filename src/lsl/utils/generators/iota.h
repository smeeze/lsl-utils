#pragma once

namespace lsl {
namespace utils {
namespace generators {
class iota {
public:
  struct context {
  };
  iota(context &&ctx) : _context(ctx) {}
  void operator()(int, int, int, float *, int, int &);

private:
  context _context;
};
} // namespace generators
} // namespace utils
} // namespace lsl
