#pragma once

// fmt
#include <fmt/format.h>

namespace lsl {
namespace utils {
namespace generators {
class debug {
public:
  struct context {};
  debug(context &&) {}
  void operator()(int sample_counter, int s, int rate, const float *,
                  int channel_count, int &) {
    fmt::print("{}({}, {}, {}, ... , {}, ...)\n", __PRETTY_FUNCTION__,
               sample_counter, s, rate, channel_count);
  }
};
} // namespace generators
} // namespace utils
} // namespace lsl
