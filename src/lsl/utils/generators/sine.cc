// system
#include <cmath>
// lsl utils
#include <lsl/utils/generators/sine.h>
///////////////////////////////////////////////////////////////////////////////
void lsl::utils::generators::sine::operator()(int, int s, int rate,
                                              float *channel_data,
                                              int channel_count, int &trigger) {
  const float value(0.1 * _context.scaling *
                    sinf(2.0f * 3.14159265359f * float(s) / float(rate)));
  float *b(channel_data);
  float *e(channel_data + channel_count);
  while (b != e) {
    *b = value;
    ++b;
  }
  if (s == 0) {
    trigger = _context.trigger_code;
    if (_context.trigger_increment) {
      ++_context.trigger_code;
    }
  } else {
    trigger = 0;
  }
}
