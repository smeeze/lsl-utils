#pragma once

// lsl utils libeep
#include <lsl/utils/libeep/factory.h>

namespace lsl {
namespace utils {
namespace generators {
class cnt {
public:
  struct context {
    std::string filename;
  };
  cnt(context &&ctx) : _context(ctx) {
    _cnt_reader =
        lsl::utils::libeep::factory::make_cnt_reader(_context.filename);
  }
  void operator()(int, int, int, float *, int, int &);

private:
  context _context;
  std::unique_ptr<lsl::utils::libeep::cnt_reader> _cnt_reader;
};
} // namespace generators
} // namespace utils
} // namespace lsl
