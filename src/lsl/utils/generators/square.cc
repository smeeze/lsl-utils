// lsl utils
#include <lsl/utils/generators/square.h>
///////////////////////////////////////////////////////////////////////////////
void lsl::utils::generators::square::operator()(int, int s, int rate,
                                                float *channel_data,
                                                int channel_count,
                                                int &trigger) {
  const float f(0.1 * _context.scaling);
  const float value((s < (rate * 30 / 100)) ? f : -f);
  float *b(channel_data);
  float *e(channel_data + channel_count);
  while (b != e) {
    *b = value;
    ++b;
  }
  if (s == 0) {
    trigger = _context.trigger_code;
    if (_context.trigger_increment) {
      ++_context.trigger_code;
    }
  } else {
    trigger = 0;
  }
}
