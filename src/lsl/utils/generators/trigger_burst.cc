// system
#include <cmath>
// lsl utils
#include <lsl/utils/generators/trigger_burst.h>
///////////////////////////////////////////////////////////////////////////////
void lsl::utils::generators::trigger_burst::operator()(int, int, int,
                                                       float *channel_data,
                                                       int channel_count,
                                                       int &trigger) {
  const float value(0.1 * _context.scaling * float(_interval_counter));
  float *b(channel_data);
  float *e(channel_data + channel_count);
  while (b != e) {
    *b = value;
    ++b;
  }

  ++_interval_counter;
  if (_interval_counter > _context.trigger_interval) {
    _interval_counter = 0;
    trigger = _context.trigger_code;
  } else {
    trigger = 0;
  }
}
