#pragma once

namespace lsl {
namespace utils {
namespace generators {
class trigger_burst {
public:
  struct context {
    int trigger_code;
    int trigger_interval;
    float scaling;
  };
  trigger_burst(context &&ctx) : _context(ctx) {}
  void operator()(int, int, int, float *, int, int &);

private:
  context _context;
  int _interval_counter=0;
};
} // namespace generators
} // namespace utils
} // namespace lsl
