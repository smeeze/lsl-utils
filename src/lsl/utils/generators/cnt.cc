// fmt
#include <fmt/format.h>
// lsl utils
#include <lsl/utils/generators/cnt.h>
///////////////////////////////////////////////////////////////////////////////
void lsl::utils::generators::cnt::operator()(int, int, int,
                                             float *sample_buffer,
                                             int sample_buffer_size,
                                             int &trigger) {
  _cnt_reader->pull(sample_buffer, sample_buffer_size, trigger);
}
