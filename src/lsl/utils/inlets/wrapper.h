#pragma once

// lsl
#include <lsl_cpp.h>
// lsl utils
#include <lsl/utils/formatter.h>

namespace lsl {
namespace utils {
namespace inlets {
template <typename Handler> class wrapper {
public:
  wrapper(const lsl::stream_info &stream_info)
      : _lsl_stream_info(stream_info),
        _lsl_inlet(std::make_unique<lsl::stream_inlet>(stream_info)), _n(0) {
    _channel_format = _lsl_stream_info.channel_format();
    _channel_count = _lsl_stream_info.channel_count();
    fmt::print("no postprocessing\n");
    // _lsl_inlet->set_postprocessing();
  }

protected:
  void _pull() const {
    switch (_channel_format) {
    case lsl::channel_format_t::cf_float32:
      _pull_loop<float>();
      break;
    case lsl::channel_format_t::cf_double64:
      _pull_loop<double>();
      break;
    case lsl::channel_format_t::cf_string:
      _pull_loop<std::string>();
      break;
    case lsl::channel_format_t::cf_int32:
      _pull_loop<int32_t>();
      break;
    case lsl::channel_format_t::cf_int16:
      _pull_loop<int16_t>();
      break;
    case lsl::channel_format_t::cf_int8:
      _pull_loop<char>();
      break;
    case lsl::channel_format_t::cf_int64:
    case lsl::channel_format_t::cf_undefined:
      throw(std::runtime_error(
          fmt::format("unhandled format: {}", _channel_format)));
      break;
    }
  }

protected:
  template <typename T> void _pull_loop() const {
    const auto clock_correction(_lsl_inlet->time_correction());
    const double timeout(0.01);

    std::vector<T> sample;

    auto clock = _pull_sample(sample, timeout);
    while (clock >= timeout) {
      const auto local_clock(lsl::local_clock());
      static_cast<const Handler *>(this)->handle(_n, _lsl_stream_info.uid(),
                                                 local_clock, clock, _old_clock,
                                                 clock_correction, sample);
      _old_clock = clock;
      clock = _pull_sample(sample, timeout);
      ++_n;
    }
  }

  template <typename T> double _pull_sample(T &t, double timeout) const {
    return _lsl_inlet->pull_sample(t, timeout);
  }

private:
  lsl::stream_info _lsl_stream_info;
  std::unique_ptr<lsl::stream_inlet> _lsl_inlet;
  lsl::channel_format_t _channel_format;
  int _channel_count;
  mutable uint32_t _n;
  mutable double _old_clock;
};
} // namespace inlets
} // namespace utils
} // namespace lsl
