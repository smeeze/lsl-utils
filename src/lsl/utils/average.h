#pragma once

// system
#include <array>

namespace lsl {
namespace utils {
template <typename T, size_t N> class average {
public:
  average() : _values({0}) {}

  void add_value(const T &t) {
    _sum -= _values[_index];
    _values[_index] = t;
    _sum += _values[_index];
    _index = (_index + 1) % N;
  }

  T get() const { return _sum / (T)N; }

private:
  std::array<T, N> _values;
  T _sum = 0;
  size_t _index = 0;
};
} // namespace utils
} // namespace lsl
