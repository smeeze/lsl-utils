#pragma once

// system
#include <memory>
// lsl
#include <lsl_cpp.h>
// lsl utils
#include <lsl/utils/outlets/stats.h>
#include <lsl/utils/simulator/context.h>

namespace lsl {
namespace utils {
namespace outlets {
class basic {
public:
  struct context {
    std::string serial;
    std::string mode;
    double clock_skew;
  };
  basic(context &&ctx) : _context(std::forward<context>(ctx)) {}

  void init(const lsl::utils::simulator::context &);
  void prepare();
  void push(const float *, int, int32_t);

  const lsl::utils::outlets::stats &get_stats() const { return _stats; }

protected:
  context _context;
  lsl::utils::outlets::stats _stats;
  int _block_counter;
  int _sample_rate;

protected:
  std::unique_ptr<lsl::stream_outlet> _lsl_samples_outlet;
  std::unique_ptr<lsl::stream_outlet> _lsl_markers_outlet;
};
} // namespace outlets
} // namespace utils
} // namespace lsl
