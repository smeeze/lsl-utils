// fmt
#include <fmt/format.h>
// lsl utils
#include <lsl/utils/outlets/basic.h>
///////////////////////////////////////////////////////////////////////////////
void lsl::utils::outlets::basic::init(
    const lsl::utils::simulator::context &context) {
  const int stream_channel_count(context.channel_count +
                                 (context.with_trigger_channel ? 1 : 0) +
                                 (context.with_counter_channel ? 1 : 0));
  lsl::stream_info lsl_samples_info(_context.serial, _context.mode,
                                    stream_channel_count, context.sample_rate,
                                    lsl::cf_float32, _context.serial);
  _sample_rate = context.sample_rate;

  if (true) {
    lsl::xml_element chns = lsl_samples_info.desc().append_child("channels");
    int channel_index(0);
    while (channel_index < context.channel_count) {
      for (int c = 0; c < context.channel_count; ++c) {
        std::string type("ref");
        if (channel_index >= context.aux_offset) {
          type = "aux";
        }
        chns.append_child("channel")
            .append_child_value("index", fmt::format("{}", channel_index))
            .append_child_value("unit", "unit")
            .append_child_value("type", type);
        ++channel_index;
      }
      if (context.with_trigger_channel) {
        chns.append_child("channel")
            .append_child_value("index", fmt::format("{}", channel_index))
            .append_child_value("unit", "none")
            .append_child_value("type", "trigger");

        ++channel_index;
      }
      if (context.with_counter_channel) {
        chns.append_child("channel")
            .append_child_value("index", fmt::format("{}", channel_index))
            .append_child_value("unit", "none")
            .append_child_value("type", "counter");

        ++channel_index;
      }
    }
  }
  _lsl_samples_outlet = std::make_unique<lsl::stream_outlet>(lsl_samples_info);

  // trigger
  if (context.with_trigger_stream) {
    const lsl::stream_info lsl_markers_info(_context.serial, "Markers", 1,
                                            lsl::IRREGULAR_RATE, lsl::cf_int32,
                                            _context.serial);
    _lsl_markers_outlet =
        std::make_unique<lsl::stream_outlet>(lsl_markers_info);
  }
}
///////////////////////////////////////////////////////////////////////////////
void lsl::utils::outlets::basic::prepare() {
  _stats.clock = lsl::local_clock() + _context.clock_skew;
  _block_counter = 0;
}
///////////////////////////////////////////////////////////////////////////////
void lsl::utils::outlets::basic::push(const float *sample_data, int,
                                      int32_t trigger) {
  double stamp(_stats.clock + (double)_block_counter / (double)_sample_rate);
  ++_block_counter;
  // samples
  _lsl_samples_outlet->push_sample(sample_data, stamp);
  _stats.samples_consumed = _lsl_samples_outlet->have_consumers();
  // markers
  if (_lsl_markers_outlet && trigger) {
    _lsl_markers_outlet->push_sample(&trigger, stamp);
    _stats.markers_consumed = _lsl_markers_outlet->have_consumers();
  }
}
