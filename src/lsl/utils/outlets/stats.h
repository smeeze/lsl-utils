#pragma once

namespace lsl {
namespace utils {
namespace outlets {
struct stats {
  double clock;
  bool samples_consumed = false;
  bool markers_consumed = false;
};
} // namespace outlets
} // namespace utils
} // namespace lsl
