#pragma once

// system
#include <chrono>
// lsl utils
#include <lsl/utils/simulator/base.h>
#include <lsl/utils/simulator/context.h>

namespace lsl {
namespace utils {
namespace simulator {
template <typename Generator, typename Outlet>
class basic : public lsl::utils::simulator::base {
public:
  basic(const simulator::context &ctx, typename Generator::context &&gc,
        typename Outlet::context &&oc)
      : _context(ctx),
        _generator(std::forward<typename Generator::context>(gc)),
        _outlet(std::forward<typename Outlet::context>(oc)) {}

  void start() override;
  void step() override;

  const lsl::utils::outlets::stats &get_stats() const override {
    return _outlet.get_stats();
  }

protected:
  context _context;
  Generator _generator;
  Outlet _outlet;

protected:
  std::chrono::high_resolution_clock::time_point _stamp;
  int _sample_counter;
  std::vector<float> _multiplex_buffer;
};
} // namespace simulator
} // namespace utils
} // namespace lsl
