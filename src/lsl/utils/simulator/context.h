#pragma once

namespace lsl {
namespace utils {
namespace simulator {
struct context {
  int sample_rate;
  int channel_count;
  int aux_offset;
  bool with_trigger_channel;
  bool with_counter_channel;
  bool with_trigger_stream;
  float sample_rate_error_factor=1.0;
};
} // namespace simulator
} // namespace utils
} // namespace lsl
