#pragma once

// lsl utils
#include <lsl/utils/outlets/stats.h>

namespace lsl {
namespace utils {
namespace simulator {
class base {
public:
  virtual ~base() {}

  virtual void start() = 0;
  virtual void step() = 0;

  virtual const lsl::utils::outlets::stats &get_stats() const = 0;
};
} // namespace simulator
} // namespace utils
} // namespace lsl
