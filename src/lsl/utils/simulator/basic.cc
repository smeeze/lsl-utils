// fmt
#include <fmt/format.h>
// lsl util
#include <lsl/utils/generators/cnt.h>
#include <lsl/utils/generators/debug.h>
#include <lsl/utils/generators/iota.h>
#include <lsl/utils/generators/sine.h>
#include <lsl/utils/generators/square.h>
#include <lsl/utils/generators/trigger_burst.h>
#include <lsl/utils/outlets/basic.h>
#include <lsl/utils/simulator/basic.h>
///////////////////////////////////////////////////////////////////////////////
template class lsl::utils::simulator::basic<lsl::utils::generators::debug,
                                            lsl::utils::outlets::basic>;
template class lsl::utils::simulator::basic<lsl::utils::generators::cnt,
                                            lsl::utils::outlets::basic>;
template class lsl::utils::simulator::basic<lsl::utils::generators::iota,
                                            lsl::utils::outlets::basic>;
template class lsl::utils::simulator::basic<lsl::utils::generators::sine,
                                            lsl::utils::outlets::basic>;
template class lsl::utils::simulator::basic<lsl::utils::generators::square,
                                            lsl::utils::outlets::basic>;
template class lsl::utils::simulator::basic<
    lsl::utils::generators::trigger_burst, lsl::utils::outlets::basic>;
///////////////////////////////////////////////////////////////////////////////
template <typename Generator, typename Outlet>
void lsl::utils::simulator::basic<Generator, Outlet>::start() {
  _stamp = std::chrono::high_resolution_clock::now();
  _sample_counter = 0;
  _multiplex_buffer.resize(_context.channel_count +
                           (_context.with_trigger_channel ? 1 : 0) +
                           (_context.with_counter_channel ? 1 : 0));

  _outlet.init(_context);
}
///////////////////////////////////////////////////////////////////////////////
template <typename Generator, typename Outlet>
void lsl::utils::simulator::basic<Generator, Outlet>::step() {
  const auto stamp(std::chrono::high_resolution_clock::now());
  const auto delta_us_since_start(
      std::chrono::duration_cast<std::chrono::microseconds>(stamp - _stamp)
          .count());
  const auto counter(
      delta_us_since_start *
      (int)((float)_context.sample_rate * _context.sample_rate_error_factor) /
      1e6);

  _outlet.prepare();
  while (_sample_counter < counter) {
    int trigger(0);
    _generator(_sample_counter, _sample_counter % _context.sample_rate,
               _context.sample_rate, _multiplex_buffer.data(),
               _context.channel_count, trigger);

    // extra channels
    int sample_index(_context.channel_count);
    if (_context.with_trigger_channel) {
      _multiplex_buffer[sample_index] = trigger;
      ++sample_index;
    }
    if (_context.with_counter_channel) {
      // truncate sample counter, rollover once in a million
      _multiplex_buffer[sample_index] = (float)((uint16_t)_sample_counter);
      ++sample_index;
    }
    ++_sample_counter;

    _outlet.push(_multiplex_buffer.data(), _multiplex_buffer.size(), trigger);
  }
}
