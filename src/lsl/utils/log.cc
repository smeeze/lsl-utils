// system
#include <chrono>
#include <vector>
// boost
#include <boost/algorithm/string.hpp>
// fmt
#include <fmt/chrono.h>
#include <fmt/core.h>
// erptools
#include <lsl/utils/log.h>
///////////////////////////////////////////////////////////////////////////////
namespace fmt {
template <> struct formatter<lsl::utils::log::level> : formatter<string_view> {
  template <typename FormatContext>
  auto format(const lsl::utils::log::level &l, FormatContext &ctx) const {
    std::string_view name = "unknown";
    switch (l) {
    case lsl::utils::log::level::console:
      name = "con";
      break;
    case lsl::utils::log::level::debug:
      name = "dbg";
      break;
    case lsl::utils::log::level::info:
      name = "inf";
      break;
    case lsl::utils::log::level::warning:
      name = "wrn";
      break;
    case lsl::utils::log::level::error:
      name = "err";
      break;
    case lsl::utils::log::level::fatal:
      name = "fat";
      break;
    case lsl::utils::log::level::todo:
      name = "tdo";
      break;
    }
    return formatter<string_view>::format(name, ctx);
  }
};
} // namespace fmt
///////////////////////////////////////////////////////////////////////////////
void lsl::utils::log::_log(lsl::utils::log::level lvl, const std::string &msg) {
  assert(!msg.empty());
  assert(msg.back() != '\n');
  const auto stamp(std::chrono::system_clock::now());
  auto stamp_string(
      fmt::format("{0:}.{1:%S}", stamp, stamp.time_since_epoch()));

  std::vector<std::string> msg_split;
  boost::split(msg_split, msg, boost::is_any_of("\r\n"));

  for (const auto &m : msg_split) {
    if (m.empty()) {
      continue;
    }
    fmt::print("{} [{}]: {}\n", stamp_string, lvl, m);
    for (auto &i : stamp_string) {
      i = ' ';
    }
  }
}
