#pragma once

// fmt
#include <fmt/format.h>
// lsl
#include <lsl_cpp.h>

namespace fmt {
template <> struct formatter<lsl::channel_format_t> : formatter<string_view> {
  template <typename FormatContext>
  auto format(const lsl::channel_format_t &f, FormatContext &ctx) const {
    std::string_view name = "unknown";
    switch (f) {
    case lsl::channel_format_t::cf_float32:
      name = "float32";
      break;
    case lsl::channel_format_t::cf_double64:
      name = "double64";
      break;
    case lsl::channel_format_t::cf_string:
      name = "string";
      break;
    case lsl::channel_format_t::cf_int32:
      name = "int32";
      break;
    case lsl::channel_format_t::cf_int16:
      name = "int16";
      break;
    case lsl::channel_format_t::cf_int8:
      name = "int8";
      break;
    case lsl::channel_format_t::cf_int64:
      name = "int64";
      break;
    case lsl::channel_format_t::cf_undefined:
      name = "undefined";
      break;
    }
    return formatter<string_view>::format(name, ctx);
  }
};
} // namespace fmt
