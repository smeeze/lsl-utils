#pragma once

namespace lsl {
namespace utils {
namespace version {
constexpr int major() { return 0; }
constexpr int minor() { return 2; }
constexpr int micro() { return 4; }
} // namespace version
} // namespace utils
} // namespace lsl
