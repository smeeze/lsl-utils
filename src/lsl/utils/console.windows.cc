// system
#include <conio.h>
#include <Windows.h>
// self
#include <lsl/utils/console.h>
///////////////////////////////////////////////////////////////////////////////
int lsl::utils::console::has_key_press() {
  return _kbhit();
}
///////////////////////////////////////////////////////////////////////////////
int lsl::utils::console::get_key_press() {
  return _getch();
}
