#pragma once

// system
#include <algorithm>
#include <functional>
#include <vector>
// LSL
#include <lsl_cpp.h>

namespace lsl {
namespace utils {
namespace resolve {
class poll {
public:
  template <typename... Args>
  poll(Args &&... args) : _lsl_resolver(std::forward<Args>(args)...) {}
  virtual ~poll() {}

  void operator()() {
    auto lsl_streams(_lsl_resolver.results());

    _check_detach(lsl_streams);
    _check_attach(lsl_streams);

    // set for next loop
    _lsl_streams.clear();
    // _lsl_streams = lsl_streams;
    swap(_lsl_streams, lsl_streams);
  }

protected:
  virtual void _add(const lsl::stream_info &) {}
  virtual void _del(const lsl::stream_info &) {}

  virtual bool _has_bypass(const lsl::stream_info &) { return false; }

private:
  void _check_attach(const std::vector<lsl::stream_info> &lsl_streams) {
    for (const auto &l : lsl_streams) {
      if (!_has_bypass(l) &&
          std::find_if(std::begin(_lsl_streams), std::end(_lsl_streams),
                       [&l](const lsl::stream_info &si) {
                         return l.uid() == si.uid();
                       }) == std::end(_lsl_streams)) {
        _add(l);
      }
    }
  }
  void _check_detach(const std::vector<lsl::stream_info> &lsl_streams) {
    for (const auto &l : _lsl_streams) {
      if (std::find_if(std::begin(lsl_streams), std::end(lsl_streams),
                       [&l](const lsl::stream_info &si) {
                         return l.uid() == si.uid();
                       }) == std::end(lsl_streams)) {
        _del(l);
      }
    }
  }

private:
  lsl::continuous_resolver _lsl_resolver;
  std::vector<lsl::stream_info> _lsl_streams;
};
} // namespace resolve
} // namespace utils
} // namespace lsl
