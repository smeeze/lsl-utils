#pragma once

namespace lsl {
namespace utils {
class console {
public:
  static int has_key_press();
  static int get_key_press();
};
} // namespace utils
} // namespace lsl
