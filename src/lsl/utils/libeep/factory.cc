// system
#include <cstdlib>
// system
#include <unordered_map>
// libeep
extern "C" {
#include <v4/eep.h>
}
// fmt
#include <fmt/format.h>
// lsl utils libeep
#include <lsl/utils/libeep/factory.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
//
//
//
class _cnt_reader : public lsl::utils::libeep::cnt_reader {
public:
  _cnt_reader(const std::string &filename) {
    _cnt_handle = libeep_read_with_external_triggers(filename.c_str());
    if (_cnt_handle < 0) {
      throw(std::runtime_error(
          fmt::format("could not open cnt file {}", filename)));
    }

    // cache values
    _cnt_channel_count = libeep_get_channel_count(_cnt_handle);
    _cnt_sample_count = libeep_get_sample_count(_cnt_handle);
    // TODO triggers
    for (int n = 0; n < libeep_get_trigger_count(_cnt_handle); ++n) {
      uint64_t offset;
      auto code(libeep_get_trigger(_cnt_handle, n, &offset));
      _trigger_map[offset] = std::atoi(code);
    }
  }

  void pull(float *write_buffer, int write_buffer_size, int &trigger) override {
    float *cnt_sample(libeep_get_samples(_cnt_handle, _cnt_sample_index,
                                         _cnt_sample_index + 1));
    for (int i = 0; i < write_buffer_size; ++i) {
      write_buffer[i] = cnt_sample[i % _cnt_channel_count] * 0.01f;
    }
    libeep_free_samples(cnt_sample);

    const auto f(_trigger_map.find(_cnt_sample_index));
    if (f == std::end(_trigger_map)) {
      trigger = 0;
    } else {
      trigger = f->second;
    }

    ++_cnt_sample_index;
    if (_cnt_sample_index >= _cnt_sample_count) {
      _cnt_sample_index = 0;
    }
  }

private:
  cntfile_t _cnt_handle;
  int _cnt_channel_count;
  long _cnt_sample_count;
  long _cnt_sample_index = 0;
  std::unordered_map<uint64_t, int> _trigger_map;
};
//
//
//
class _cnt_writer : public lsl::utils::libeep::cnt_writer {
public:
  _cnt_writer(const std::string &filename, int rate, int channel_count) {
    const auto channel_info(libeep_create_channel_info());
    for (int channel_id = 0; channel_id < channel_count; ++channel_id) {
      const auto label(fmt::format("chan{:03}", channel_id));
      libeep_add_channel(channel_info, label.c_str(), "", "uV");
    }
    _cnt_handle = libeep_write_cnt(filename.c_str(), rate, channel_info, 1);
    if (_cnt_handle < 0) {
      throw(std::runtime_error(
          fmt::format("could not open cnt file {}", filename)));
    }
  }

  ~_cnt_writer() { libeep_close(_cnt_handle); }
  void push(const std::vector<float> &data) override {
    libeep_add_samples(_cnt_handle, data.data(), 1);
  }

private:
  cntfile_t _cnt_handle;
};
//
//
//
class _factory {
public:
  _factory() { libeep_init(); }
  ~_factory() { libeep_exit(); }

  std::unique_ptr<lsl::utils::libeep::cnt_reader>
  make_cnt_reader(const std::string &filename) {
    return std::make_unique<_cnt_reader>(filename);
  }

  std::unique_ptr<lsl::utils::libeep::cnt_writer>
  make_cnt_writer(const std::string &filename, int rate, int channel_count) {
    return std::make_unique<_cnt_writer>(filename, rate, channel_count);
  }

private:
};
//
//
//
_factory &get_factory() {
  static _factory _factory_instance;
  return _factory_instance;
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
std::unique_ptr<lsl::utils::libeep::cnt_reader>
lsl::utils::libeep::factory::make_cnt_reader(const std::string &filename) {
  return get_factory().make_cnt_reader(filename);
}
///////////////////////////////////////////////////////////////////////////////
std::unique_ptr<lsl::utils::libeep::cnt_writer>
lsl::utils::libeep::factory::make_cnt_writer(const std::string &filename,
                                             int rate, int channel_count) {
  return get_factory().make_cnt_writer(filename, rate, channel_count);
}
