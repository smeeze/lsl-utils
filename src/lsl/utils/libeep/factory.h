#pragma once

// system
#include <memory>
// lsl utils libeep
#include <lsl/utils/libeep/cnt_reader.h>
#include <lsl/utils/libeep/cnt_writer.h>

namespace lsl {
namespace utils {
namespace libeep {
class factory {
public:
  static std::unique_ptr<lsl::utils::libeep::cnt_reader>
  make_cnt_reader(const std::string &);
  static std::unique_ptr<lsl::utils::libeep::cnt_writer>
  make_cnt_writer(const std::string &, int, int);
};
} // namespace libeep
} // namespace utils
} // namespace lsl
