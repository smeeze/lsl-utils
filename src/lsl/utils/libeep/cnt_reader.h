#pragma once

// system
#include <string>

namespace lsl {
namespace utils {
namespace libeep {
class cnt_reader {
public:
  virtual ~cnt_reader() {}
  virtual void pull(float *sample_buffer, int sample_buffer_size,
                    int &trigger) = 0;
};
} // namespace libeep
} // namespace utils
} // namespace lsl
