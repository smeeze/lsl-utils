#pragma once

// system
#include <vector>

namespace lsl {
namespace utils {
namespace libeep {
class cnt_writer {
public:
  virtual ~cnt_writer() {}
  virtual void push(const std::vector<float> &data) = 0;
};
} // namespace libeep
} // namespace utils
} // namespace lsl
