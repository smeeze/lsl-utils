// system
#include <chrono>
#include <iostream>
#include <thread>
// boost
#include <boost/program_options.hpp>
// fmt
#include <fmt/format.h>
#include <fmt/ostream.h>
// lsl utils
#include <lsl/utils/console.h>
#include <lsl/utils/generators/cnt.h>
#include <lsl/utils/generators/debug.h>
#include <lsl/utils/generators/iota.h>
#include <lsl/utils/generators/sine.h>
#include <lsl/utils/generators/square.h>
#include <lsl/utils/generators/trigger_burst.h>
#include <lsl/utils/log.h>
#include <lsl/utils/outlets/basic.h>
#include <lsl/utils/simulator/base.h>
#include <lsl/utils/simulator/basic.h>
#include <lsl/utils/version.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
std::unique_ptr<lsl::utils::simulator::base>
make_simulator(const lsl::utils::simulator::context &context,
               bool trigger_increment, int trigger_code, int trigger_interval,
               const std::string &generator, const std::string &serial,
               const std::string &mode, float clock_skew, float scaling,
               const std::string &filename) {
  //
  if (generator == "cnt") {
    using simulator = lsl::utils::simulator::basic<lsl::utils::generators::cnt,
                                                   lsl::utils::outlets::basic>;
    return std::unique_ptr<simulator>(
        new simulator(context, {filename}, {serial, mode, clock_skew}));
  }
  //
  if (generator == "iota") {
    using simulator = lsl::utils::simulator::basic<lsl::utils::generators::iota,
                                                   lsl::utils::outlets::basic>;
    return std::unique_ptr<simulator>(
        new simulator(context, {}, {serial, mode, clock_skew}));
  }
  //
  if (generator == "sine") {
    using simulator = lsl::utils::simulator::basic<lsl::utils::generators::sine,
                                                   lsl::utils::outlets::basic>;
    return std::unique_ptr<simulator>(
        new simulator(context, {trigger_increment, trigger_code, scaling},
                      {serial, mode, clock_skew}));
  }
  //
  if (generator == "square") {
    using simulator =
        lsl::utils::simulator::basic<lsl::utils::generators::square,
                                     lsl::utils::outlets::basic>;
    return std::unique_ptr<simulator>(
        new simulator(context, {trigger_increment, trigger_code, scaling},
                      {serial, mode, clock_skew}));
  }
  //
  if (generator == "trigger-burst") {
    using simulator =
        lsl::utils::simulator::basic<lsl::utils::generators::trigger_burst,
                                     lsl::utils::outlets::basic>;
    return std::unique_ptr<simulator>(
        new simulator(context, {trigger_code, trigger_interval, scaling},
                      {serial, mode, clock_skew}));
  }

  throw(std::runtime_error(fmt::format("unknown generator '{}'", generator)));
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
  lsl::utils::log::console(
      "{}, version {}.{}.{}", argv[0], lsl::utils::version::major(),
      lsl::utils::version::minor(), lsl::utils::version::micro());
  try {
    // options
    lsl::utils::simulator::context context;
    bool trigger_increment;
    int trigger_code;
    int trigger_interval;
    std::string generator;
    std::string serial;
    std::string mode;
    float clock_skew;
    float scaling;
    std::string filename;
    int update_frequency(10);

    boost::program_options::options_description desc("options");
    desc.add_options()
        // help
        ("help,h", "produce help message")
        // sample rate
        ("sample-rate",
         boost::program_options::value<int>(&context.sample_rate)
             ->default_value(512),
         "sample rate")
        // channel count
        ("channel-count",
         boost::program_options::value<int>(&context.channel_count)
             ->default_value(8),
         "channel count")
        // aux offset
        ("aux-offset",
         boost::program_options::value<int>(&context.aux_offset)
             ->default_value(64),
         "aux offset")
        // trigger increment
        ("trigger-increment",
         boost::program_options::bool_switch(&trigger_increment)
             ->default_value(false),
         "increment trigger value")
        // trigger code
        ("trigger-code",
         boost::program_options::value<int>(&trigger_code)->default_value(1),
         "trigger code")
        // trigger interval
        ("trigger-interval",
         boost::program_options::value<int>(&trigger_interval)
             ->default_value(context.sample_rate),
         "trigger interval")
        // trigger channel
        ("with-trigger-channel",
         boost::program_options::bool_switch(&context.with_trigger_channel)
             ->default_value(false),
         "with trigger channel")
        // counter channel
        ("with-counter-channel",
         boost::program_options::bool_switch(&context.with_counter_channel)
             ->default_value(false),
         "with counter channel")
        // trigger stream
        ("with-trigger-stream",
         boost::program_options::bool_switch(&context.with_trigger_stream)
             ->default_value(false),
         "with trigger stream")
        // sample rate error factor
        ("sample-rate-error-factor",
         boost::program_options::value<float>(&context.sample_rate_error_factor)
             ->default_value(1.0),
         "sample rate error factor")
        // generator
        ("generator",
         boost::program_options::value<std::string>(&generator)
             ->default_value("square"),
         "generator(cnt, iota, sine, square, trigger-burst)")
        // serial
        ("serial",
         boost::program_options::value<std::string>(&serial)->default_value(
             "1234"),
         "serial")
        // mode
        ("mode",
         boost::program_options::value<std::string>(&mode)->default_value(
             "EEG"),
         "mode")
        // clock skew
        ("clock-skew",
         boost::program_options::value<float>(&clock_skew)->default_value(0.0),
         "clock skew")
        // scaling
        ("scaling",
         boost::program_options::value<float>(&scaling)->default_value(1.0),
         "scaling")
        // filename
        ("filename",
         boost::program_options::value<std::string>(&filename)->default_value(
             ""),
         "filename")
        // update frequency
        ("update-frequency",
         boost::program_options::value<int>(&update_frequency)
             ->default_value(8),
         "update frequency");

    boost::program_options::variables_map vm;
    boost::program_options::store(
        boost::program_options::command_line_parser(argc, argv)
            .options(desc)
            .run(),
        vm);

    boost::program_options::notify(vm);

    if (vm.count("help")) {
      desc.print(std::cout);
      return 0;
    }
    //
    // overview
    //
    lsl::utils::log::console("settings:");
    lsl::utils::log::console("  sample rate........... {}",
                             context.sample_rate);
    lsl::utils::log::console("  channel count......... {}",
                             context.channel_count);
    lsl::utils::log::console("  aux offset............ {}", context.aux_offset);
    lsl::utils::log::console("  with trigger channel.. {}",
                             context.with_trigger_channel);
    lsl::utils::log::console("  with counter channel.. {}",
                             context.with_counter_channel);
    lsl::utils::log::console("  with trigger stream... {}",
                             context.with_trigger_stream);
    lsl::utils::log::console("  update frequency...... {}", update_frequency);
    lsl::utils::log::console("  generator............. {}", generator);
    lsl::utils::log::console("  serial................ {}", serial);
    lsl::utils::log::console("  mode.................. {}", mode);
    lsl::utils::log::console("  clock skew............ {}", clock_skew);
    lsl::utils::log::console("  scaling............... {}", scaling);
    lsl::utils::log::console("  filename.............. {}", filename);

    auto simulator(make_simulator(context, trigger_increment, trigger_code,
                                  trigger_interval, generator, serial, mode,
                                  clock_skew, scaling, filename));

    lsl::utils::log::console("streaming...");
    auto stamp(std::chrono::high_resolution_clock::now());
    simulator->start();
    while (!lsl::utils::console::has_key_press()) {
      stamp += std::chrono::milliseconds(1000 / update_frequency);
      std::this_thread::sleep_until(stamp);
      simulator->step();
      const auto stats(simulator->get_stats());
      fmt::print(
          "\r[clock: {:.4f}] [samples consumed: {}] [markers consumed: {}]...",
          stats.clock, stats.samples_consumed, stats.markers_consumed);
    }
  } catch (const std::exception &e) {
    lsl::utils::log::error("error in main: {}", e.what());
  }

  return 0;
}
