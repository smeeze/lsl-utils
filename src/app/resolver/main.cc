// system
#include <chrono>
#include <functional>
#include <thread>
// fmt
#include <fmt/chrono.h>
#include <fmt/format.h>
// lsl
#include <lsl_cpp.h>
// lsl utils
#include <lsl/utils/console.h>
#include <lsl/utils/formatter.h>
#include <lsl/utils/version.h>
///////////////////////////////////////////////////////////////////////////////
#define USE_CONTINUOUS_RESOLVER
///////////////////////////////////////////////////////////////////////////////
namespace {
//
class poller {
public:
  using attach_callback = std::function<void(const lsl::stream_info &)>;
  using detach_callback = std::function<void(const lsl::stream_info &)>;

  void operator()(const attach_callback &attach,
                  const detach_callback &detach) {
#ifdef USE_CONTINUOUS_RESOLVER
    auto lsl_streams(_lsl_resolver.results());
#else
    std::vector<lsl::stream_info> lsl_streams(lsl::resolve_streams());
#endif

    _check_detach(lsl_streams, detach);
    _check_attach(lsl_streams, attach);

    // set for next loop
    _lsl_streams.clear();
    // _lsl_streams = lsl_streams;
    swap(_lsl_streams, lsl_streams);
  }

private:
  void _check_attach(const std::vector<lsl::stream_info> &lsl_streams,
                     const attach_callback &attach) {
    for (const auto &l : lsl_streams) {
      if (std::find_if(std::begin(_lsl_streams), std::end(_lsl_streams),
                       [&l](const lsl::stream_info &si) {
                         return l.uid() == si.uid();
                       }) == std::end(_lsl_streams)) {
        // send callback
        attach(l);
      }
    }
  }
  void _check_detach(const std::vector<lsl::stream_info> &lsl_streams,
                     const detach_callback &detach) {
    for (const auto &l : _lsl_streams) {
      if (std::find_if(std::begin(lsl_streams), std::end(lsl_streams),
                       [&l](const lsl::stream_info &si) {
                         return l.uid() == si.uid();
                       }) == std::end(lsl_streams)) {
        // send callback
        detach(l);
      }
    }
  }

private:
#ifdef USE_CONTINUOUS_RESOLVER
  lsl::continuous_resolver _lsl_resolver;
#endif
  std::vector<lsl::stream_info> _lsl_streams;
};
} // namespace
///////////////////////////////////////////////////////////////////////////////
int main(int, char **argv) {
  fmt::print("{}, version {}.{}.{}\n", argv[0], lsl::utils::version::major(),
             lsl::utils::version::minor(), lsl::utils::version::micro());
  try {
    poller p;
    auto stamp(std::chrono::high_resolution_clock::now());
    while (!lsl::utils::console::has_key_press()) {
      stamp += std::chrono::milliseconds(100);
      std::this_thread::sleep_until(stamp);
      p(
          [](const lsl::stream_info &si) {
            fmt::print("attached({}):\n", std::chrono::system_clock::now());
            fmt::print(" uid............ {}\n", si.uid());
            fmt::print(" name........... {}\n", si.name());
            fmt::print(" type........... {}\n", si.type());
            fmt::print(" source id...... {}\n", si.source_id());
            fmt::print(" nominal rate... {}\n", si.nominal_srate());
            fmt::print(" channel fmt.... {}\n", si.channel_format());
            fmt::print(" channel count.. {}\n", si.channel_count());
            fmt::print(" hostname....... {}\n", si.hostname());
            fmt::print("{}\n", si.as_xml());

            lsl::stream_inlet lsl_inlet(si);
            fmt::print("  inlet:\n");
            fmt::print("  time correction.... {}\n",
                       lsl_inlet.time_correction());
            fmt::print("  samples available.. {}\n",
                       lsl_inlet.samples_available());
            fmt::print("  channel count...... {}\n",
                       lsl_inlet.get_channel_count());
            fmt::print("{}\n", lsl_inlet.info().as_xml());
          },
          [](const lsl::stream_info &si) {
            fmt::print("detached({}): {} {} {} {} {}\n",
                       std::chrono::system_clock::now(), si.uid(),
                       si.hostname(), si.type(), si.name(), si.source_id());
          });
    }
  } catch (const std::exception &e) {
    fmt::print(stderr, "error: {}\n", e.what());
  }
  return 0;
}
