// system
#include <iostream>
// boost
#include <boost/program_options.hpp>
// fmt
#include <fmt/format.h>
// lsl
#include <lsl_cpp.h>
// lsl utils
#include <lsl/utils/formatter.h>
#include <lsl/utils/version.h>
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
  fmt::print("{}, version {}.{}.{}\n", argv[0], lsl::utils::version::major(),
             lsl::utils::version::minor(), lsl::utils::version::micro());
  try {
    //
    // resolve
    //
    bool dump_raw;
    int resolve_count;
    double resolve_timeout;
    std::string resolve_predicate;
    boost::program_options::options_description desc("options");
    desc.add_options()
        // help
        ("help,h", "produce help message")
        // dump raw
        ("dump-raw",
         boost::program_options::bool_switch(&dump_raw)->default_value(false),
         "dump raw info")
        // resolve count
        ("resolve-count",
         boost::program_options::value<int>(&resolve_count)->default_value(1),
         "resolve count")
        // resolve timeout
        ("resolve-timeout",
         boost::program_options::value<double>(&resolve_timeout)
             ->default_value(60.0),
         "resolve timeout")
        // predicate
        ("resolve-predicate",
         boost::program_options::value<std::string>(&resolve_predicate)
             ->default_value("*"),
         "resolve predicate");
    ;

    boost::program_options::variables_map vm;
    boost::program_options::store(
        boost::program_options::command_line_parser(argc, argv)
            .options(desc)
            .run(),
        vm);

    boost::program_options::notify(vm);

    if (vm.count("help")) {
      desc.print(std::cout);
      return 0;
    }
    //
    // resolve
    fmt::print("resolving...\n");
    std::vector<lsl::stream_info> stream_infos(
        lsl::resolve_stream(resolve_predicate, resolve_count, resolve_timeout));
    fmt::print("resolve done.\n");
    //
    // info
    //
    for (const auto &si : stream_infos) {
      fmt::print("-------------------------------------------------------------"
                 "------------------\n");
      fmt::print("stream info:\n");
      fmt::print(" uid............ {}\n", si.uid());
      fmt::print(" name........... {}\n", si.name());
      fmt::print(" type........... {}\n", si.type());
      fmt::print(" source id...... {}\n", si.source_id());
      fmt::print(" channel fmt.... {}\n", si.channel_format());
      fmt::print(" channel count.. {}\n", si.channel_count());
      fmt::print(" hostname....... {}\n", si.hostname());
      if (dump_raw) {
        fmt::print("{}\n", si.as_xml());
      }

      lsl::stream_inlet lsl_inlet(si);
      fmt::print("  inlet:\n");
      fmt::print("  time correction.... {}\n", lsl_inlet.time_correction());
      fmt::print("  samples available.. {}\n", lsl_inlet.samples_available());
      fmt::print("  channel count...... {}\n", lsl_inlet.get_channel_count());
      if (dump_raw) {
        fmt::print("{}\n", lsl_inlet.info().as_xml());
      }
    }
  } catch (const std::exception &e) {
    fmt::print(stderr, "error: {}\n", e.what());
  }
  return 0;
}
