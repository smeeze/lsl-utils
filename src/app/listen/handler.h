#pragma once

namespace app {
namespace listen {
class handler {
public:
  virtual ~handler() {}
  virtual void pull() = 0;
};

} // namespace listen
} // namespace app
