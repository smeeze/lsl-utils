#pragma once

// self
#include <app/listen/handler.h>
#include <lsl/utils/inlets/wrapper.h>
#include <lsl/utils/libeep/factory.h>

namespace app {
namespace listen {
namespace handlers {
class cnt : public handler, public lsl::utils::inlets::wrapper<cnt> {
public:
  template <typename... Args>
  cnt(const std::string &type, const std::string &id, int rate,
      int channel_count, Args &&... args)
      : lsl::utils::inlets::wrapper<cnt>(std::forward<Args>(args)...) {
    const auto filename(fmt::format("LSL-{}-{}.cnt", type, id));
    _cnt_writer = lsl::utils::libeep::factory::make_cnt_writer(filename, rate,
                                                               channel_count);
  }
  void pull() override { _pull(); }

  void handle(uint32_t, const std::string &, double, double, double, double,
              const std::vector<float> &data) const {
    _cnt_writer->push(data);
  }
  template <typename T>
  void handle(uint32_t, const std::string &, double, double, double, double,
              const T &) const {
    fmt::print("no cnt handler for type {}\n", __PRETTY_FUNCTION__);
  }

private:
  std::unique_ptr<lsl::utils::libeep::cnt_writer> _cnt_writer;
};
} // namespace handlers
} // namespace listen
} // namespace app
