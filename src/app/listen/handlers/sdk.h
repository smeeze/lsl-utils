#pragma once

// system
#include <cassert>
// system
#include <string>
// self
#include <app/listen/handler.h>
#include <lsl/utils/inlets/wrapper.h>

namespace app {
namespace listen {
namespace handlers {
class sdk : public handler, public lsl::utils::inlets::wrapper<sdk> {
public:
  template <typename... Args>
  sdk(Args &&... args)
      : lsl::utils::inlets::wrapper<sdk>(std::forward<Args>(args)...) {}

  void pull() override { _pull(); }

  void handle(uint32_t, const std::string &uid, double, double, double, double,
              const std::vector<float> &sample) const {
    if (uid != _uid_last) {
      fmt::print("\n");
      _uid_last = uid;
    }
    // TODO
    assert(sample.size() >= 2);
    size_t idx(0);
    fmt::print("\r{}:", uid);
    // sample data
    for (idx = 0; idx < sample.size() - 2; ++idx) {
      fmt::print(" {: 8.5f}", sample[idx]);
    }
    // trigger
    const int trigger(sample[idx]);
    ++idx;
    // delta
    const uint16_t counter(sample[idx]);
    const uint16_t delta(counter - _last_counter);
    _last_counter = counter;
    ++idx;
    // show
    fmt::print(" [trg:{:3} delta:{:5} counter:{:5}]", trigger, delta, counter);
    // newline?
    if (trigger > 0 || delta != 1) {
      fmt::print("\n");
    }
  }
  template <typename T>
  void handle(uint32_t, const std::string &, double, double, double, double,
              const T &) const {
    fmt::print("\rnot supported:{}", __PRETTY_FUNCTION__);
  }

private:
  static std::string _uid_last;
  mutable uint64_t _last_counter;
};
} // namespace handlers
} // namespace listen
} // namespace app
