#pragma once

// system
#include <chrono>
#include <string>
// fmt
#include <fmt/core.h>
// self
#include <app/listen/handler.h>
#include <lsl/utils/average.h>
#include <lsl/utils/inlets/wrapper.h>

namespace app {
namespace listen {
namespace handlers {
class rate : public handler, public lsl::utils::inlets::wrapper<rate> {
public:
  template <typename... Args>
  rate(Args &&...args)
      : lsl::utils::inlets::wrapper<rate>(std::forward<Args>(args)...) {}

  void pull() override { _pull(); }

  template <typename T>
  void handle(uint32_t, const std::string &uid, double, double clock, double,
              double clock_correction, const T &) const {
    _averages.set(clock + clock_correction);

    const auto stamp(std::chrono::system_clock::now());
    if (std::chrono::duration_cast<std::chrono::milliseconds>(stamp - _stamp)
            .count() >= 1000) {
      _stamp = stamp;

      if (_uid_last != uid) {
        fmt::print("\n");
      }
      fmt::print(" {} {:5.2f}", uid, 1.0 / _averages.get());
    }
#if 0
    const auto stamp(std::chrono::system_clock::now());
    if (std::chrono::duration_cast<std::chrono::milliseconds>(stamp - _stamp)
            .count() >= 1000) {
      _stamp = stamp;
      // house keeping
      for (auto i = std::begin(_averages_map); i != std::end(_averages_map);) {
        fmt::print(" {} {:5.2f}", i->first, i->second.get());
        if (i->second.is_old()) {
          i = _averages_map.erase(i);
        } else {
          ++i;
        }
      }
    }
#endif
  }

private:
  class averages {
  public:
    void set(double clock) {
      _stamp_old = std::chrono::high_resolution_clock::now();

      const double delta(clock - _clock_old);
      _clock_old = clock;

      _average.add_value(delta);
    }
    double get() const { return 1.0 / _average.get(); }
    bool is_old() const {
      return std::chrono::duration_cast<std::chrono::milliseconds>(
                 std::chrono::high_resolution_clock::now() - _stamp_old)
                 .count() > 1000;
    }

  protected:
    double _clock_old;
    std::chrono::high_resolution_clock::time_point _stamp_old;
    lsl::utils::average<double, 512> _average;
  };
  mutable averages _averages;
  mutable std::chrono::system_clock::time_point _stamp;
  static std::string _uid_last;
};
} // namespace handlers
} // namespace listen
} // namespace app
