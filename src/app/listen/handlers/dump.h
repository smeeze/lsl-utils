#pragma once

// system
#include <string>
// fmt
#include <fmt/ranges.h>
// self
#include <app/listen/handler.h>
#include <lsl/utils/inlets/wrapper.h>

namespace app {
namespace listen {
namespace handlers {
class dump : public handler, public lsl::utils::inlets::wrapper<dump> {
public:
  template <typename... Args>
  dump(Args &&...args)
      : lsl::utils::inlets::wrapper<dump>(std::forward<Args>(args)...) {}

  void pull() override { _pull(); }

  template <typename T>
  void handle(uint32_t, const std::string &uid, double local_clock,
              double clock, double, double clock_correction,
              const T &sample) const {

    if (uid != _uid_last) {
      fmt::print("\n");
      _uid_last = uid;
    }
    fmt::print("\r{}:[{: .5f} {: .5f} {: .5f}] {}...", uid, clock - local_clock,
               clock, clock + clock_correction, sample);
  }

private:
  static std::string _uid_last;
};
} // namespace handlers
} // namespace listen
} // namespace app
