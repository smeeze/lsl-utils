#pragma once

// system
#include <chrono>
#include <fstream>
// self
#include <app/listen/handler.h>
#include <lsl/utils/inlets/wrapper.h>

namespace app {
namespace listen {
namespace handlers {
class timing : public handler, public lsl::utils::inlets::wrapper<timing> {
public:
  template <typename... Args>
  timing(const std::string &type, const std::string &id, Args &&...args)
      : lsl::utils::inlets::wrapper<timing>(std::forward<Args>(args)...),
        _out(fmt::format("LSL-timing-{}-{}.txt", type, id)) {
    _out << fmt::format("#clock local_clock delta correction\n");
  }
  void pull() override { _pull(); }

  template <typename T>
  void handle(uint32_t n, const std::string &, double local_clock, double clock,
              double old_clock, double clock_correction, const T &) const {
    const int interval(1000 / 10);
    const auto stamp(std::chrono::high_resolution_clock::now());
    const auto elapsed(std::chrono::duration_cast<std::chrono::milliseconds>(
                           stamp - _last_stamp)
                           .count());
    if (elapsed < 0 || elapsed >= (interval * 2)) {
      _last_stamp = stamp;
    }
    if (n > 0 && elapsed >= interval) {
      double clock_delta(clock + clock_correction - old_clock);
      _out << fmt::format("{: .15f} {: .15f} {: .15f} {: .15f}\n", clock,
                          local_clock, clock_delta, clock_correction);
      _last_stamp += std::chrono::milliseconds(interval);
    }
  }

private:
  mutable std::ofstream _out;
  mutable std::chrono::high_resolution_clock::time_point _last_stamp;
};
} // namespace handlers
} // namespace listen
} // namespace app
