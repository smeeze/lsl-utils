#pragma once

// system
#include <chrono>
// fmt
#include <fmt/format.h>
#include <fmt/ostream.h>
// self
#include <app/listen/handler.h>
#include <lsl/utils/inlets/wrapper.h>

namespace app {
namespace listen {
namespace handlers {
class timestamp : public handler,
                  public lsl::utils::inlets::wrapper<timestamp> {
public:
  template <typename... Args>
  timestamp(Args &&...args)
      : lsl::utils::inlets::wrapper<timestamp>(std::forward<Args>(args)...) {}
  void pull() override { _pull(); }

  template <typename T>
  void handle(uint32_t, const std::string &, double, double clock, double,
              double, const T &) const {
    const auto s0(std::chrono::steady_clock::now().time_since_epoch().count());
    const auto s1((double)s0 * 1e-9);
    const auto s2(clock - s1);
    fmt::print("\rclock: {:.4f} s0: {} s1: {} s2: {}", clock, s0, s1, s2);
  }
};
} // namespace handlers
} // namespace listen
} // namespace app
