// system
#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>
// boost
#include <boost/program_options.hpp>
// lsl
#include <lsl_cpp.h>
// lsl utils
#include <lsl/utils/console.h>
#include <lsl/utils/inlets/wrapper.h>
#include <lsl/utils/log.h>
#include <lsl/utils/resolve/poll.h>
#include <lsl/utils/version.h>
// self
#include <app/listen/handler.h>
#include <app/listen/handlers/cnt.h>
#include <app/listen/handlers/dump.h>
#include <app/listen/handlers/rate.h>
#include <app/listen/handlers/sdk.h>
#include <app/listen/handlers/timestamp.h>
#include <app/listen/handlers/timing.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
std::string mode;
std::string resolve_predicate;

std::mutex lsl_wrappers_mutex;
std::unordered_map<std::string, std::unique_ptr<app::listen::handler>>
    lsl_wrappers;
} // namespace
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
  lsl::utils::log::console(
      "{}, version {}.{}.{}", argv[0], lsl::utils::version::major(),
      lsl::utils::version::minor(), lsl::utils::version::micro());
  try {
    // options
    boost::program_options::options_description desc("options");
    desc.add_options()
        // help
        ("help,h", "produce help message")
        // mode
        ("mode",
         boost::program_options::value<std::string>(&mode)->default_value(
             "dump"),
         "mode(dump, timing, timestamp, cnt, sdk, rate)")
        // predicate
        ("resolve-predicate",
         boost::program_options::value<std::string>(&resolve_predicate)
             ->default_value("*"),
         "resolve predicate");
    boost::program_options::variables_map vm;
    boost::program_options::store(
        boost::program_options::command_line_parser(argc, argv)
            .options(desc)
            .run(),
        vm);

    boost::program_options::notify(vm);

    if (vm.count("help")) {
      desc.print(std::cout);
      // lsl::utils::log::console("{}", desc.);
      return 0;
    }
    //
    // resolve
    //
    lsl::utils::log::console("resolving...");
    lsl::utils::log::console("  mode....... {}", mode);
    lsl::utils::log::console("  predicate.. {}", resolve_predicate);

    //
    // create clients
    //
    class my_poll : public lsl::utils::resolve::poll {
    protected:
      using poll::poll;

      void _add(const lsl::stream_info &si) override {
        lsl::utils::log::console("add stream. type={} name={} source_id={}",
                                 si.type(), si.name(), si.source_id());

        std::lock_guard<std::mutex> lsl_wrappers_lock_guard{lsl_wrappers_mutex};

        if (mode == "dump") {
          lsl_wrappers[si.uid()] =
              std::make_unique<app::listen::handlers::dump>(si);
        }
        if (mode == "sdk") {
          lsl_wrappers[si.uid()] =
              std::make_unique<app::listen::handlers::sdk>(si);
        }
        if (mode == "rate") {
          lsl_wrappers[si.uid()] =
              std::make_unique<app::listen::handlers::rate>(si);
        }
        if (mode == "timing") {
          lsl_wrappers[si.uid()] =
              std::make_unique<app::listen::handlers::timing>(si.type(),
                                                              si.name(), si);
        }
        if (mode == "timestamp") {
          lsl_wrappers[si.uid()] =
              std::make_unique<app::listen::handlers::timestamp>(si);
        }
        if (mode == "cnt") {
          try {
            lsl_wrappers[si.uid()] =
                std::make_unique<app::listen::handlers::cnt>(
                    si.type(), si.name(), si.nominal_srate(),
                    si.channel_count(), si);
          } catch (...) {
          }
        }
      }
      void _del(const lsl::stream_info &si) override {
        lsl::utils::log::console("del stream {} {} {}", si.type(), si.name(),
                                 si.source_id());

        std::lock_guard<std::mutex> lsl_wrappers_lock_guard{lsl_wrappers_mutex};

        lsl_wrappers.erase(si.uid());
      }
    };

    my_poll my_poll_instance(resolve_predicate);
    bool my_poll_run(true);
    std::thread my_poll_thread([&]() {
      auto stamp(std::chrono::high_resolution_clock::now());
      while (my_poll_run) {
        my_poll_instance();
        stamp += std::chrono::milliseconds(1000);
        std::this_thread::sleep_until(stamp);
      }
    });

    //
    // loop
    //
    auto stamp(std::chrono::high_resolution_clock::now());
    while (!lsl::utils::console::has_key_press()) {
      stamp += std::chrono::milliseconds(250);
      std::this_thread::sleep_until(stamp);
      std::lock_guard<std::mutex> lsl_wrappers_lock_guard{lsl_wrappers_mutex};
      for (const auto &w : lsl_wrappers) {
        w.second->pull();
      }
    }
    //
    // close
    //
    lsl::utils::log::console("closing...");
    my_poll_run = false;
    my_poll_thread.join();
    lsl::utils::log::console("done.");
  } catch (const std::exception &e) {
    lsl::utils::log::error("error: {}", e.what());
    return -1;
  }
  return 0;
}
