cmake_minimum_required(VERSION 3.15)

project(lsl-utils)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

if(EXISTS ${CMAKE_SOURCE_DIR}/user.cmake)
   include(${CMAKE_SOURCE_DIR}/user.cmake)
endif()

include(FetchContent)

include(cmake/ccache.cmake)
include(cmake/clang.cmake)
include(cmake/gcc.cmake)
include(cmake/msvc.cmake)

include(3rdparty/libeep.cmake)

find_package(Boost REQUIRED)
find_package(Fmt REQUIRED)
find_package(LSL REQUIRED)

add_library(lsl-utils-libeep STATIC
  src/lsl/utils/libeep/factory.cc
)
target_include_directories(lsl-utils-libeep PRIVATE src)
target_link_libraries(lsl-utils-libeep PRIVATE EepStatic)
target_link_libraries(lsl-utils-libeep PRIVATE warnings::high)
target_link_libraries(lsl-utils-libeep PRIVATE fmt::fmt)

add_library(lsl-utils STATIC
  src/lsl/utils/average.h
  src/lsl/utils/console.h
  src/lsl/utils/formatter.h
  src/lsl/utils/generators/cnt.cc
  src/lsl/utils/generators/cnt.h
  src/lsl/utils/generators/debug.h
  src/lsl/utils/generators/iota.cc
  src/lsl/utils/generators/iota.h
  src/lsl/utils/generators/sine.cc
  src/lsl/utils/generators/sine.h
  src/lsl/utils/generators/square.cc
  src/lsl/utils/generators/square.h
  src/lsl/utils/generators/trigger_burst.cc
  src/lsl/utils/generators/trigger_burst.h
  src/lsl/utils/inlets/wrapper.h
  src/lsl/utils/log.cc
  src/lsl/utils/log.h
  src/lsl/utils/outlets/basic.cc
  src/lsl/utils/outlets/basic.h
  src/lsl/utils/outlets/stats.h
  src/lsl/utils/resolve/poll.h
  src/lsl/utils/simulator/base.h
  src/lsl/utils/simulator/basic.cc
  src/lsl/utils/simulator/basic.h
  src/lsl/utils/simulator/context.h
  src/lsl/utils/version.h
)
target_include_directories(lsl-utils        PUBLIC src)
target_link_libraries(lsl-utils PUBLIC lsl-utils-libeep)
target_link_libraries(lsl-utils PUBLIC warnings::high)
target_link_libraries(lsl-utils PUBLIC boost::boost)
target_link_libraries(lsl-utils PUBLIC fmt::fmt)
target_link_libraries(lsl-utils PUBLIC LSL::lsl)
if(UNIX)
  target_sources(lsl-utils PRIVATE src/lsl/utils/console.linux.cc)
endif()
if(WIN32)
  target_sources(lsl-utils PRIVATE src/lsl/utils/console.windows.cc)
endif()

add_executable(app-list src/app/list/main.cc)
target_link_libraries(app-list PRIVATE lsl-utils)
install(TARGETS app-list DESTINATION bin)

add_executable(app-listen
  src/app/listen/handlers/dump.cc
  src/app/listen/handlers/dump.h
  src/app/listen/handlers/rate.cc
  src/app/listen/handlers/rate.h
  src/app/listen/handlers/sdk.cc
  src/app/listen/handlers/sdk.h
  src/app/listen/handlers/timing.h
  src/app/listen/main.cc
)
target_link_libraries(app-listen PRIVATE lsl-utils)
install(TARGETS app-listen DESTINATION bin)

add_executable(app-stream src/app/stream/main.cc)
target_link_libraries(app-stream PRIVATE lsl-utils)
install(TARGETS app-stream DESTINATION bin)

add_executable(app-resolver src/app/resolver/main.cc)
target_link_libraries(app-resolver PRIVATE lsl-utils)
install(TARGETS app-resolver DESTINATION bin)

# add_executable(app-cascade src/app/cascade/main.cc)
# target_link_libraries(app-cascade PRIVATE lsl-utils)
# install(TARGETS app-cascade DESTINATION bin)

# local test stuff
file(GLOB test_dirs test/misc/*)
foreach(d ${test_dirs})
  if(IS_DIRECTORY ${d})
    get_filename_component(dn ${d} NAME)
    file(GLOB test_misc_${dn}_sources ${d}/*.h ${d}/*.cc)
    if(test_misc_${dn}_sources)
      message("found test application...... test_misc_${dn}")
      add_executable(test_misc_${dn} ${test_misc_${dn}_sources})
      target_link_libraries(test_misc_${dn} PRIVATE lsl-utils)
      target_link_libraries(test_misc_${dn} PRIVATE warnings::high)
    endif()
  endif()
endforeach()
