FetchContent_Declare(
  libeep
  GIT_REPOSITORY https://gitlab.com/smeeze/libeep
)

FetchContent_GetProperties(libeep)
if(NOT libeep_POPULATED)
  MESSAGE("need to populate libeep...")
  FetchContent_Populate(libeep)
  add_subdirectory(${libeep_SOURCE_DIR} ${libeep_BINARY_DIR} EXCLUDE_FROM_ALL)
  MESSAGE("done populating libeep")
endif()
