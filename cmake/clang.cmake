if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror=all")
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror=extra")
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror=weak-vtables")

  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fvisibility=hidden -fvisibility-inlines-hidden")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fdata-sections")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -ffunction-sections")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wl,--gc-sections")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wl,-s")

  add_library(warnings_high INTERFACE)
  add_library(warnings::high ALIAS warnings_high)
  target_compile_options(warnings_high INTERFACE 
    -Werror=all;-Werror=extra
  )
endif() # Clang
