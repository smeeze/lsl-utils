if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  add_library(warnings_high INTERFACE)
  add_library(warnings::high ALIAS warnings_high)
  target_compile_options(warnings_high INTERFACE 
    -Werror=all;-Werror=extra;-Werror=non-virtual-dtor;-Werror=reorder;-Werror=return-local-addr;-Werror=suggest-override
  )

  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fvisibility=hidden -fvisibility-inlines-hidden")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fdata-sections")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -ffunction-sections")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wl,--gc-sections")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wl,-s")
endif() # GNU
