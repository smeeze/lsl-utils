if(WIN32)
# add_definitions(/W3)
# add_definitions(/wd4251)
# add_definitions(/wd4996)
  add_definitions(-D__PRETTY_FUNCTION__=__FUNCSIG__)
# add_definitions(/MP)
  add_definitions(-DWIN32_LEAN_AND_MEAN)

  add_library(warnings_high INTERFACE)
  add_library(warnings::high ALIAS warnings_high)
  target_compile_options(warnings_high INTERFACE /W4)
endif() # WIN32
